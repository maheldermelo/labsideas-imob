var forever = require('forever-monitor');

  var child = new (forever.Monitor)('cluster.js', {
    max: 3,
    silent: false,
    watch: false,
    killTree: true,
    logFile: './logs/logFile', // Path to log output from forever process (when daemonized)
    outFile: './logs/outFile', // Path to log output from child stdout
    errFile: './logs/errFile', // Path to log output from child stderr
    args: []
  });

  child.on('exit', function () {
    console.log('your-filename.js has exited after 3 restarts');
  });

  child.start();
