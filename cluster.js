var cluster = require('cluster');
var http = require('http');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
  // Fork workers.
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('listening', function(worker) {
      console.log("Cluster %d conectado", worker.process.pid);
  });
  cluster.on('disconnect', function(worker) {
      console.log('Cluster %d esta desconectado.', worker.process.pid);
  });
  cluster.on('exit', function(worker) {
      console.log('Cluster %d caiu fora.', worker.process.pid);
  });

} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  console.log('Request to worker %d', cluster.worker.id);
  require('./bin/www');
}
