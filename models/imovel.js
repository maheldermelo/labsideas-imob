var mongoose = require('mongoose');

module.exports = function(){
  var schema = mongoose.Schema({
    description: {
      type: 'String',
      required: true
    },
    price:{
      type: 'Number',
      required: true
    }
  });

  return mongoose.model('Imovel', schema);
};
